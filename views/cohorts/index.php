<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 09/04/19
 * Time: 14:34
 */
defined('MOODLE_INTERNAL') || die();

/*
 * Variables :
 * - school of class school_table
 * - cohorts (id, name, size?) of class app_cohort
 * - cohortlink:  cohort page link
 * - mainpagelink
 * - tutorial
 */

?>
<header class="local">
    <div class="title">
        <h2><?php echo $school->name ?></h2>
        <p><?php echo $school->city . ' - ' . strtoupper($school->uai) ?></p>
    </div>

    <nav class="navbar navbar-default">
        <ul class="nav navbar-nav">
            <li>
                <a href="<?php echo $mainpagelink ?>">
                    <span class="glyphicon glyphicon-chevron-left">&nbsp;</span>
                    <?php echo get_string('mainpagelink', 'local_massimilate')?>
                </a>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li>
                <a href="mailto:support-elea@ac-versailles.fr">
                    <span class="glyphicon glyphicon-envelope">&nbsp;</span>Écrire au support
                </a>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li>
                <a href="http://www.viaeduc.fr/group/10245" target="_blank">
                    <span class="glyphicon glyphicon-question-sign">&nbsp;</span>Échanger entre pairs
                </a>
            </li>
        </ul>
        <?php if (isset($tutorial)) : ?>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="<?php echo $tutorial ?>" target="_blank">
                        <span class="glyphicon glyphicon-info-sign">&nbsp;</span>Accéder aux tutoriels
                    </a>
                </li>
            </ul>
        <?php endif ?>
    </nav>

</header>

<div class="row tiles text-center">
    <?php foreach ($cohorts as $cohort) : ?>
        <div class="col-sm-6 col-md-4 col-lg-3 cohort-link">
            <a href="<?php echo $cohortlink . $cohort->name?>">
                <div class="tile cohort-tile">
                    <h2><?php echo $cohort->name ?></h2>
                </div>
            </a>
        </div>
    <?php endforeach ?>
</div>
