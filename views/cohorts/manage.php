<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 09/04/19
 * Time: 14:34
 */
defined('MOODLE_INTERNAL') || die();

global $PAGE;

$PAGE->requires->js(new moodle_url('/local/massimilate/public/js/deleteCohortMember.js'));

/*
 * Variables :
 * - cohort
 * - cohortmembers (set of users)
 * - mainpagelink
 * - cohortslink
 * - tutorial
 */

?>


<header class="local">
    <div class="title">
        <h2><?php echo $cohort->name ?></h2>
    </div>

    <nav class="navbar navbar-default">
        <ul class="nav navbar-nav">
            <li>
                <a href="<?php echo $cohortslink ?>">
                    <span class="glyphicon glyphicon-chevron-left">&nbsp;</span>
                    <?php echo get_string('cohortmanagementlink', 'local_massimilate')?>
                </a>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li>
                <a href="mailto:support-elea@ac-versailles.fr">
                    <span class="glyphicon glyphicon-envelope">&nbsp;</span>Écrire au support
                </a>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li>
                <a href="http://www.viaeduc.fr/group/10245" target="_blank">
                    <span class="glyphicon glyphicon-question-sign">&nbsp;</span>Échanger entre pairs
                </a>
            </li>
        </ul>
        <?php if (isset($tutorial)) : ?>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="<?php echo $tutorial ?>" target="_blank">
                        <span class="glyphicon glyphicon-info-sign">&nbsp;</span>Accéder aux tutoriels
                    </a>
                </li>
            </ul>
        <?php endif ?>
    </nav>
</header>

<div >
    <?php foreach ($cohortmembers as $member) : ?>
        <div class="col-sm-4 col-md-3 col-lg-2">
            <div class="tile tile-user" data-userid="<?php echo $member->id ?>">
                <span class="close delete-member"
                      data-cohortid="<?php echo $cohort->id ?>"
                      data-cohortname="<?php echo $cohort->name ?>"
                      data-userid="<?php echo $member->id ?>"
                      data-username="<?php echo strtoupper($member->lastname) . ' ' . $member->firstname ?>"
                      title="<?php echo get_string('cohortmemberdeletiontitle', 'local_massimilate')?>">
                    X
                </span>
                <div>
                    <h2>
                        <div><?php echo strtoupper($member->lastname) ?></div>
                        <div><?php echo $member->firstname ?></div>
                    </h2>
                </div>
            </div>
        </div>
    <?php endforeach ?>
</div>

<div class="modal fade course-modal" id="cohortmember-deletion" tabindex='-1'>
    <div class="modal-dialog">
        <div class="modal-content container-fluid cohort-modal-content">

            <div class="modal-header cohort-modal-header">
                <button type="button" class="close course-modal-close" data-dismiss="modal">x</button>
                <div>
                    <h3>
                        <?php echo get_string('cohortmemberdeletiontitle', 'local_massimilate')?>
                    </h3>
                </div>
            </div>
            <div class="modal-body cohort-modal-body">
                <div id="cohortmember-deletion-body">
                    <?php echo get_string('cohortmemberdeletiontext', 'local_massimilate')?>
                </div>
                <div id="cohortmember-deletion-template" style="display:none">
                    <?php echo get_string('cohortmemberdeletiontext', 'local_massimilate')?>
                </div>
                <button id="cohortmember-deletion-confirmation" type="button" class="btn btn-secondary">Retirer</button>
            </div>
        </div>
    </div>
</div>

