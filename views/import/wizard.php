<?php

// This file is part of Massimilate.
// 
// Massimilate is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Massimilate is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Massimilate.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Importation wizard view
 *
 * @package     local_massimilate
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

global $PAGE;

$PAGE->requires->js(new moodle_url('/local/massimilate/public/js/prototype/ManageSteps.js'));
$PAGE->requires->js(new moodle_url('/local/massimilate/public/js/prototype/ManagePasswords.js'));
$PAGE->requires->js(new moodle_url('/local/massimilate/public/js/zip/zip.js'));
$PAGE->requires->js(new moodle_url('/local/massimilate/public/js/zip/mime-types.js'));
$PAGE->requires->js(new moodle_url('/local/massimilate/public/js/bcrypt/bCrypt.js'));
$PAGE->requires->js(new moodle_url('/local/massimilate/public/js/bcrypt/isaac.js'));
$PAGE->requires->js(new moodle_url('/local/massimilate/public/js/importWizard.js'));

?>

<script src="/local/massimilate/public/js/pdfmake/pdfmake.min.js"></script>
<script src="/local/massimilate/public/js/pdfmake/vfs_fonts.min.js"></script>

<h2>Importer des utilisateurs</h2>

<div class="well">
    
    <div class="hidden-xs" id="mooring-navbar">
        <ul class="nav nav-pills row">
            <li class="active col-sm-3" id="nav-upload">
                <a href="">
                    <h4 class="list-group-item-heading">Étape 1</h4>
                    <p class="list-group-item-text">Dépôt du fichier</p>
                </a>
            </li>
            <li class="disabled col-sm-3" id="nav-cohorts">
                <a href="">
                    <h4 class="list-group-item-heading">Étape 2</h4>
                    <p class="list-group-item-text">Importation des classes</p>
                </a>
            </li>
            <li class="disabled col-sm-3" id="nav-users">
                <a href="">
                    <h4 class="list-group-item-heading">Étape 3</h4>
                    <p class="list-group-item-text">Importation des utilisateurs</p>
                </a>
            </li>
            <li class="disabled col-sm-3" id="nav-report">
                <a href="">
                    <h4 class="list-group-item-heading">Étape 4</h4>
                    <p class="list-group-item-text">Rapport d'activité</p>
                </a>
            </li>
        </ul>
        <hr>
        <br>
    </div>
    
    <div id="step-upload">
        <form class="mform">
            <div class="fitem">
                <div class="fitemtitle">Sélectionnez ou déposez le fichier <br> que vous avez exporté depuis votre ENT <br> ou - à défaut - depuis l'application SIECLE</div>
                <div class="felement">
                    <input type="button" value="Choisir un fichier..." id="xmlfile-btn">
                    <div class="dropfile-system">
                        <div class="dropfile-container" id="xmlfile-container">
                            <input type="file" class="dropfile" id="xmlfile" />
                            <label for="dropfile">
                                <img src="public/img/dnd_arrow.gif"><br>
                                Vous pouvez glisser le fichier ici !
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <form class="mform" style="display:none" id="step-upload-validate">
            <div class="fitem">
                <div class="fitemtitle">Veuillez lire les informations ci-contre<br> puis lancer la mise à jour de la base</div>
                <div class="felement">
                    <div class="panel">
                        <div class="panel-heading">
                          <h3 class="panel-title"></h3>
                        </div>
                        <div class="panel-body"></div>
                    </div>
                    <button type="button" id="registration-btn" class="btn-success" style="display:none">Créer les comptes sans modifier les mots de passe existants</button>
                    <button type="button" id="updtpasswd-btn" class="btn-warning" style="display:none">Créer les comptes en générant de nouveaux mots de passe pour tous</button>
                </div>
            </div>
        </form>
    </div>

    <!--list-group-item-success
    list-group-item-warning
    list-group-item-danger-->

    <div style="display:none" id="step-cohorts">
        <div class="container">
            <div class="row">
                <ul class="list-group col-sm-6 col-md-3"></ul>
                <ul class="list-group col-sm-6 col-md-3"></ul>
                <ul class="list-group col-sm-6 col-md-3"></ul>
                <ul class="list-group col-sm-6 col-md-3"></ul>
            </div>
        </div>
        <br><br>
        <div class="jumbotron">
            <div class="container text-center">
                <p>Chaque enseignant retrouvera la totalité des classes 
                    et groupes de l'établissement dans son espace
                </p>
            </div>
        </div>
    </div>

    <form class="mform" style="display:none" id="step-users">
        <div class="fitem">
            <div class="fitemtitle">
                <span class="badge" id="nbremain">0</span>
                <strong id="textremain">utilisateurs en cours de traitement...</strong>
            </div>
            <div class="felement">
                <div class="progress" id="users">
                    <div class="progress-bar progress-bar-success"></div>
                    <div class="progress-bar progress-bar-warning"></div>
                    <div class="progress-bar progress-bar-danger"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="alert col-md-offset-2 col-md-2 alert-success">
                <span class="badge" id="nbsuccess">0</span>
                <strong>utilisateurs actualisés</strong>
            </div>
            <div class="alert col-md-offset-1 col-md-2 alert-warning">
                <span class="badge" id="nbwarning">0</span>
                <strong>utilisateurs créés</strong>
            </div>
            <div class="alert col-md-offset-1 col-md-2 alert-danger">
                <span class="badge" id="nbdanger">0</span>
                <strong>utilisateurs protégés</strong>
            </div>
        </div>
        <div class="jumbotron">
            <div class="container text-center">
                <p>Les utilisateurs pourront accéder à la plateforme dans quelques instants</p>
            </div>
        </div>
    </form>

    <div class="container" style="display:none" id="step-report">
        <div class="jumbotron col-md-offset-2 col-md-8 text-center">
            <h2>Importation terminée</h2>
            <p>
                L'importation s'est déroulée avec succès et tous vos utilisateurs 
                peuvent dès à présent accéder à la plateforme.
            </p>
            <p>
                Obtenez plus d'informations sur les opérations effectuées 
                en cliquant sur les différents onglets ci-dessus.
            </p>
            <div style="display:none" id="report-ids">
                <div class="alert alert-danger" role="alert">
                    De nouveaux identifiants ont été générés. Vous devez 
                    absolument les enregistrer ou les imprimer.
                    Vous ne pourrez pas le faire plus tard&nbsp;!
                    <br><br>
                    <button type="button" class="btn-default" id="save-btn">Enregistrer</button>
                    <button type="button" class="btn-default" id="print-btn">Imprimer</button>
                </div>
            </div>
            <button type="button" class="btn-primary" id="new-btn">Importer d'autres utilisateurs</button>
            <a href="<?php echo $link ?>">
                <button type="button" class="btn-default" id="quit-btn">Revenir à la page d'accueil</button>
            </a>
        </div>
    </div>
    
    <div style="display:none" id="equal-than-prev" class="alert alert-danger text-center">
        Reprise d'une importation ayant été interrompue ! Les utilisateurs dits "protégés" ont été correctement traités précédemment.
    </div>
    
</div>