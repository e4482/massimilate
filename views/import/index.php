<?php

// This file is part of Massimilate.
// 
// Massimilate is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Massimilate is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Massimilate.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Importation index view
 *
 * @package     local_massimilate
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

?>

<header class="local">
    
    <div class="title">
        <h2><?php echo $school->name ?></h2>
        <p><?php echo $school->city . ' - ' . strtoupper($school->uai) ?></p>
    </div>
    
    <nav class="navbar navbar-default">
        <ul class="nav navbar-nav">
            <li>
                <a href="<?php echo $importlink ?>">
                    <span class="glyphicon glyphicon-plus-sign">&nbsp;</span>Réaliser une nouvelle importation
                </a>
            </li>
            <li>
                <a href="<?php echo $cohortslink ?>">
                    <span class="glyphicon glyphicon-facetime-video glyphicon-th-list">&nbsp;</span>
                    <?php echo get_string('cohortmanagementlink', 'local_massimilate')?>
                </a>
            </li>
        </ul>
        <!--<form class="navbar-form">
            <div class="input-group">
                <label class="input-group-addon" for="search"><span class="glyphicon glyphicon-search"></span></label>
                <input type="search" placeholder="Chercher un établissement" class="form-control" id="search">
            </div>
        </form>-->
        <ul class="nav navbar-nav navbar-right">
            <li>
                <a href="mailto:support-elea@ac-versailles.fr">
                    <span class="glyphicon glyphicon-envelope">&nbsp;</span>Écrire au support
                </a>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li>
                <a href="http://www.viaeduc.fr/group/10245" target="_blank">
                    <span class="glyphicon glyphicon-question-sign">&nbsp;</span>Échanger entre pairs
                </a>
            </li>
        </ul>
        <?php if (isset($tutorial)) : ?>
        <ul class="nav navbar-nav navbar-right">
            <li>
                <a href="<?php echo $tutorial ?>" target="_blank">
                    <span class="glyphicon glyphicon-info-sign">&nbsp;</span>Accéder aux tutoriels
                </a>
            </li>
        </ul>
        <?php endif ?>
    </nav>
    
</header>

<!--<h2>Statistiques de l'établissement</h2>-->

<h2>Dernières importations</h2>

<div class="row tiles text-center">
    <?php foreach ($imports as $import) : ?>
    <div class="col-sm-6 col-md-4 col-lg-3">
        <div class="tile alert-<?php echo $import->class ?>">
            <h2><?php echo $import->name ?></h2><h2><?php echo $import->timeago ?></h2>
        </div>
    </div>
    <?php endforeach ?>
</div>

<div class="row text-center">
    <a class="btn btn-default" href="<?php echo $importlink ?>">
        <h2>Nouvelle importation</h2>
    </a>
</div>
