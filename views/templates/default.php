<?php

// This file is part of Massimilate.
// 
// Massimilate is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Massimilate is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Massimilate.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Default template
 *
 * @package     local_massimilate
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

global $PAGE, $OUTPUT;

$PAGE->set_context(context_system::instance());
$PAGE->set_url(new moodle_url('/local/massimilate/index.php'));

$PAGE->requires->jquery();
$PAGE->requires->css(new moodle_url('/local/mooring/public/css/mooring.css'));
$PAGE->requires->css(new moodle_url('/local/massimilate/public/css/massimilate.css'));

$PAGE->set_pagelayout('standard');
$PAGE->set_title(get_string('title','local_massimilate'));
$PAGE->set_heading(get_string('heading','local_massimilate'));

echo $OUTPUT->header();
echo $content;
echo $OUTPUT->footer();