<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 11/04/19
 * Time: 09:46
 */
?>
<header class="local">

    <div class="title">
        <h2><?php echo $school->name ?></h2>
<p><?php echo $school->city . ' - ' . strtoupper($school->uai) ?></p>
</div>

<nav class="navbar navbar-default">
    <ul class="nav navbar-nav">
        <li>
            <a href="<?php echo $importlink ?>">
                <span class="glyphicon glyphicon-plus-sign">&nbsp;</span>Réaliser une nouvelle importation
            </a>
        </li>
        <li>
            <a href="<?php echo $cohortslink ?>">
                <span class="glyphicon glyphicon-plus-sign">&nbsp;</span>Gérer les cohortes
            </a>
        </li>
    </ul>
    <!--<form class="navbar-form">
        <div class="input-group">
            <label class="input-group-addon" for="search"><span class="glyphicon glyphicon-search"></span></label>
            <input type="search" placeholder="Chercher un établissement" class="form-control" id="search">
        </div>
    </form>-->
    <ul class="nav navbar-nav navbar-right">
        <li>
            <a href="mailto:support-elea@ac-versailles.fr">
                <span class="glyphicon glyphicon-envelope">&nbsp;</span>Écrire au support
            </a>
        </li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
        <li>
            <a href="http://www.viaeduc.fr/group/10245" target="_blank">
                <span class="glyphicon glyphicon-question-sign">&nbsp;</span>Échanger entre pairs
            </a>
        </li>
    </ul>
    <?php if (isset($tutorial)) : ?>
        <ul class="nav navbar-nav navbar-right">
            <li>
                <a href="<?php echo $tutorial ?>" target="_blank">
                    <span class="glyphicon glyphicon-info-sign">&nbsp;</span>Accéder aux tutoriels
                </a>
            </li>
        </ul>
    <?php endif ?>
</nav>

</header>