// This file is part of Massimilate.
// 
// Massimilate is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Massimilate is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Massimilate.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Importation wizard script
 *
 * @package     local_massimilate
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require(['local_mooring/ajax'], function(ajax) {

    // proprifier comme ajax.js
    zip.useWebWorkers = true
    zip.workerScriptsPath = 'public/js/zip/'

    var GO_ON = 1
    var TIMEOUT = 2000

    var ERR = {
        BAD_FILE: "Seuls les fichiers de type XML ou CSV peuvent être traités&nbsp;!",
        PARSE_ERROR: "Le fichier contient des erreurs empêchant son traitement&nbsp;!",
        DEV_NEEDED: "Un développeur doit intervenir, contactez le support&nbsp;!",
        NOT_SIECLE: "Un export SIECLE est attendu pour cet établissement&nbsp;!",
        NOT_ENTCORE: "Un export MonCollege est attendu pour cet établissement&nbsp;!",
        NOT_ITOP: "Un export ITOP est attendu pour cet établissement&nbsp;!",
        NOT_OZE: "Un export OZE est attendu pour cet établissement&nbsp;!",
        NOT_KOSMOS: "Un export KOSMOS est attendu pour cet établissement&nbsp;!",
        NOT_LILIE: "Un export monlycée.net est attendu pour cet établissement&nbsp;!",
        NOT_BE1D: "Cet export ne convient malheureusement pas&nbsp;. Votre conseiller de bassin peut vous aider&nbsp;!",
        BAD_SCHOOL: "Vous tentez d'utiliser le fichier d'un autre établissement que le vôtre&nbsp;!",
        BAD_YEAR: "L'année scolaire du fichier n'est pas l'année en cours&nbsp;!",
        BAD_ROLE: "Seuls les fichiers contenant des personnels ou des élèves sont acceptés&nbsp;!"
    }
    
    var viewModifier = Object.create(ManageSteps, {
        // construire ce tbl à partir du contenu html
        steps: {
            value: ['upload', 'cohorts', 'users', 'report']
        },

        $uploadValidate: {
            value: function(msg, authorize) {
                authorize = typeof authorize !== 'undefined' ? authorize : 0
                var type = authorize ? 'warning' : 'danger'
                var title = authorize ? "Vérification nécessaire avant de continuer" : "Extraction impossible"
                this.$stepValidate('#step-upload-validate', type, title, msg, authorize)
            }
        },

        $displayCohorts: {
            value: function(cohorts) {
                var $col = $('#step-cohorts ul')
                $col.each(function(index) {
                    for (var i = index; i < cohorts.length; i += $col.length) {
                        var newLi = '<li class="list-group-item" id="cohort-id-' + i + '">' + cohorts[i] + '</li>'
                        $(this).append(newLi)
                    }
                })
            }
        },

        $updateCohort: {
            value: function(index, status) {
                var selectID = '#cohort-id-' + index
                switch (status) {
                    case 'known':
                        $(selectID).addClass('list-group-item-success')
                        break
                    case 'added':
                        var nbaddition = Number($('#nav-cohorts span').text()) + 1
                        $('#nav-cohorts span').text(nbaddition)
                        $(selectID).addClass('list-group-item-warning')
                        break
                    default:
                        $(selectID).addClass('list-group-item-danger')
                }
            }
        },

        $updateStudentsProgress: {
            value: function(status, total) {
                switch (status) {
                    case 'updated':
                        var nb = Number($('#nbsuccess').text()) + 1
                        var percentage = nb / total * 100
                        $('#nbsuccess').text(nb)
                        $('#users .progress-bar-success').css('width', percentage + '%')
                        break
                    case 'created':
                        var nbaddition = Number($('#nav-users span').text()) + 1
                        $('#nav-users span').text(nbaddition)
                        var nb = Number($('#nbwarning').text()) + 1
                        var percentage = nb / total * 100
                        $('#nbwarning').text(nb)
                        $('#users .progress-bar-warning').css('width', percentage + '%')
                        break
                    default:
                        var nb = Number($('#nbdanger').text()) + 1
                        var percentage = nb / total * 100
                        $('#nbdanger').text(nb)
                        $('#users .progress-bar-danger').css('width', percentage + '%')
                }
                var remain = total - Number($('#nbsuccess').text()) - Number($('#nbwarning').text()) - Number($('#nbdanger').text())
                if (remain === 0) {
                    $('#nbremain').hide()
                    $('#textremain').text("L'importation est achevée")
                }
                else {
                    $('#nbremain').text(remain)
                }
            }
        },

        $adaptStepReport: {
            value: function() {
                $('#step-report #new-btn').hide()
                $('#step-report #quit-btn').hide()
                $('#step-report #report-ids').show()
            }
        }

    })

    var fileOpener = {

        loading: function(file, charset) {
            console.log(file)
            this.charset = typeof charset !== 'undefined' ? charset : 'UTF-8'
            if (typeof file.name !== 'undefined')
                var ext = file.name.substr(file.name.lastIndexOf('.') + 1).toLowerCase()
            else
                ext = 'unknown'
            if (ext === 'zip')
                return this.unZIP(file).then(this.loading.bind(this))
            if (ext === 'xml' || file.type.match('application/xml'))
                return this.parseXML(file)
            if (ext === 'csv' || file.type.match('text/csv'))
                return this.parseCSV(file, this.charset)
            return Promise.reject(Error(ERR.BAD_FILE)) 
        },

        unZIP: function(zipFile) {
            return new Promise(function(resolve, reject) {
                zip.createReader(new zip.BlobReader(zipFile), function(zipReader) {
                    zipReader.getEntries(function(entries) {
                        entries[0].getData(new zip.BlobWriter(zip.getMimeType(entries[0].filename)), function(blob) {
                            zipReader.close()
                            resolve(blob)
                        })
                    })
                }, reject)
            })

        },

        parseXML: function(file) {
            return new Promise(function(resolve, reject) {
                var reader = new FileReader()
                reader.onload = function(e) {
                    resolve(e.target.result)
                }
                reader.onerror = function(e) {
                    reject(e.target.error)
                }
                reader.readAsText(file, 'ISO-8859-15') // ISO-8859-15 pour les fichiers SIECLE uniquement
            }).then(function(content) {
                return new Promise(function(resolve, reject) {
                    // contournement permettant à Chrome et Chromium d'accepter les fichiers SIECLE
                    if (content.indexOf("Œ") === -1 && content.indexOf("œ") === -1 && content.indexOf("€") === -1) {
                        content = content.replace('encoding="ISO-8859-15"', 'encoding="ISO-8859-1"')
                        var data = new DOMParser().parseFromString(content, 'text/xml')
                        if (!data || data.getElementsByTagName("parsererror").length)
                            reject(Error(ERR.PARSE_ERROR))
                        else
                            resolve({type: 'text/xml', data: data})
                    }
                    else
                        reject(Error(ERR.DEV_NEEDED))
                    
                })
            })
        },

        parseCSV: function(file, charset) {
            return new Promise(function(resolve, reject) {
                var filename = file.name
                var lastdate = 0
                if (typeof file.lastModified !== 'undefined')
                    lastdate = file.lastModified
                else if (typeof file.lastModifiedDate !== 'undefined')
                    lastdate = file.lastModifiedDate.getTime()
                require(['local_massimilate/papaparse'], function(Papa) {
                    Papa.parse(file, {
                        encoding: charset,
                        // on fixe le délimiteur car certains imports LILIE
                        // contiennent des virgules inattendues qui posent problème
                        delimiter: ';',
                        header: true,
                        skipEmptyLines: true,
                        complete: function(data) {
                            console.log(data)
                            // certaines erreurs n'empêchant pas le bon fonctionnement de Papaparse
                            // on arrête le job uniquement s'il y a absence de donnée correcte
                            if (!data.data.length && data.errors.length)
                                reject(Error(ERR.PARSE_ERROR))
                            else {
                                resolve({type: 'text/csv', data: data, filename: filename, lastdate: lastdate})
                            }
                        }
                    })
                })
            })
        }

    }

    var dataRecognizer = {

        loading: function(allowed, parsed) {
            switch (allowed.source) {
                case 'lilie':
                    if (this.isLilieFile(parsed))
                        return this.verifyLilieFile(allowed, parsed)
                    else
                        return Promise.reject(Error(ERR.NOT_LILIE))
                    break
                case 'entcorespecial':
                    if (this.isENTCOREFile(parsed))
                        return this.verifyENTCOREFile(allowed, parsed, true)
                    else
                        return Promise.reject(Error(ERR.NOT_LILIE))
                    break
                case 'entcore':
                    if (this.isENTCOREFile(parsed))
                        return this.verifyENTCOREFile(allowed, parsed)
                    else
                        return Promise.reject(Error(ERR.NOT_ENTCORE))
                    break
                case 'itop':
                    if (this.isITOPFile(parsed))
                        return this.verifyITOPFile(allowed, parsed)
                    else
                        return Promise.reject(Error(ERR.NOT_ITOP))
                    break
                case 'oze':
                    if (this.isOZEFile(parsed))
                        return this.verifyOZEFile(allowed, parsed)
                    else
                        return Promise.reject(Error(ERR.NOT_OZE))
                    break
                case 'kosmos':
                    if (this.isKosmosFile(parsed))
                        return this.verifyKosmosFile(allowed, parsed)
                    else
                        return Promise.reject(Error(ERR.NOT_KOSMOS))
                    break
                case 'secondaire':
                    if (this.isSiecleFile(parsed))
                        return this.verifySiecleFile(allowed, parsed)
                    else
                        return Promise.reject(Error(ERR.NOT_SIECLE))
                    break
                case 'primaire':
                    if (this.isBE1DFile(parsed))
                        return this.verifyBE1DFile(parsed)
                    else
                        return Promise.reject(Error(ERR.NOT_BE1D))
                    break
            }
        },

        isSiecleFile: function(parsed) {
            if (parsed.type === 'text/xml') {
                if ($(parsed.data).find('BEE_ELEVES:first').length)
                    return true
            }
            return false
        },

        isENTCOREFile: function(parsed) {
            if (parsed.type === 'text/csv') {
                var bits = this.filenameSplitter(parsed.filename)
                if (bits.length === 2 && bits[0] === 'export')
                    return true
            }
            return false
        },

        isITOPFile: function(parsed) {
            if (parsed.type === 'text/csv') {
                var bits = this.filenameSplitter(parsed.filename)
                if (bits.length === 7 && bits[1] === 'ExportSSO' && bits[2] === 'MOODLE')
                    return true
            }
            return false
        },

        isOZEFile: function(parsed) {
            if (parsed.type === 'text/csv') {
                var bits = this.filenameSplitter(parsed.filename)
                if (bits.length === 5 && bits[0] === 'Export' && bits[1] === 'SACoche')
                    return true
            }
            return false
        },

        isKosmosFile: function(parsed) {
            if (parsed.type === 'text/csv') {
                var bits = this.filenameKosmosSplitter(parsed.filename)
                if (bits.length === 4 && bits[0] === 'ENT' && bits[1] === 'Identifiants')
                    return true
            }
            return false
        },

        isLilieFile: function(parsed) {
            if (parsed.type === 'text/csv') {
                var bits = this.filenameSplitter(parsed.filename)
                if (bits.length === 6 && bits[0] === 'liste')
                    return true
            }
            return false
        },

        isBE1DFile: function(parsed) {
            if (parsed.type === 'text/csv') {
                var bits = this.filenameSplitter(parsed.filename)
                if (bits.length === 2 && bits[0] === 'CSVExtraction')
                    return true
            }
            return false
        },

        verifySiecleFile: function(allowed, parsed) {
            return new Promise(function(resolve, reject) {
                var $params = $(parsed.data).find('PARAMETRES:first')
                var year = Number($params.find('ANNEE_SCOLAIRE').text())

                if (allowed.uai !== $params.find('UAJ').text().toLowerCase()) {
                    reject(Error(ERR.BAD_SCHOOL))
                }
                else if (!this.verifySchoolYear(year)) {
                    reject(Error(ERR.BAD_YEAR))
                }
                else {
                    var response = {}
                    var bits = this.timestringSplitter($params.find('HORODATAGE').text());
                    response.time = this.timestampPHP(new Date(bits[2], bits[1] - 1, bits[0], bits[3], bits[4], bits[5]).getTime())
                    response.type = 'eleves'
                    resolve(response)
                    this.promise = this.processSiecleFile(parsed)
                }
            }.bind(this))
        },

        verifyENTCOREFile: function(allowed, parsed, entcorespecial) {
            return new Promise(function(resolve, reject) {
                var date = new Date(parsed.lastdate)
                var y = date.getFullYear()
                var m = date.getMonth() + 1
                var year
                if (m < 8)
                    year = y - 1
                else 
                    year = y

                var special = parsed.data.data[0]['Établissements']
                console.log(special)
                if (allowed.special !== null && allowed.special !== special) {
                    reject(Error(ERR.BAD_SCHOOL))
                }
                if (!this.verifySchoolYear(year)) {
                    reject(Error(ERR.BAD_YEAR))
                }
                else {
                    var response = {}
                    if (allowed.special === null)
                        response.special = special
                    response.time = this.timestampPHP(parsed.lastdate)
                    response.type = 'tous'
                    resolve(response)
                    if (typeof entcorespecial === 'undefined')
                        this.promise = this.processENTCOREFile(parsed)
                    else
                        this.promise = this.processENTCORESPECIALFile(parsed)
                }
            }.bind(this))
        },

        verifyITOPFile: function(allowed, parsed) {
            return new Promise(function(resolve, reject) {
                var bits = this.filenameSplitter(parsed.filename)
                var date = bits[4].split('-')
                var year
                if (date[1] < 8)
                    year = date[2] - 1
                else 
                    year = date[2]

                var profil, type
                switch (bits[3]) {
                    case 'Eleve':
                        profil = 'student'
                        type = 'eleves'
                        break
                    case 'Elève':
                        profil = 'student'
                        type = 'eleves'
                        break
                    case 'Elève':
                        profil = 'student'
                        type = 'eleves'
                        break
                    case 'El%C3%A8ve':
                        profil = 'student'
                        type = 'eleves'
                        break
                    case 'Professeur':
                        profil = 'teacher'
                        type = 'profs'
                        break
                    case 'Professeur documentaliste':
                        profil = 'teacher'
                        type = 'docs'
                        break
                    case 'Documentaliste':
                        profil = 'teacher'
                        type = 'docs'
                        break
                    case 'Professeur vacataire':
                        profil = 'teacher'
                        type = 'vacataires'
                        break
                    case 'Personnel de direction':
                        profil = 'teacher'
                        type = 'direction'
                        break
                    case 'Direction':
                        profil = 'teacher'
                        type = 'direction'
                        break
                    case 'Personnel de vie scolaire':
                        profil = 'teacher'
                        type = 'viesco'
                        break
                    case 'Personnel Vie Scolaire':
                        profil = 'teacher'
                        type = 'viesco'
                        break
                    case 'CPE':
                        profil = 'teacher'
                        type = 'cpe'
                        break
                    default:
                        reject(Error(ERR.BAD_ROLE))
                        return
                }

                if (allowed.uai != bits[0].toLowerCase()) {
                    reject(Error(ERR.BAD_SCHOOL))
                }
                else if (!this.verifySchoolYear(year)) {
                    reject(Error(ERR.BAD_YEAR))
                }
                else {
                    resolve({time: this.timestampPHP(parsed.lastdate), type: type})
                    this.promise = this.processITOPFile(profil, parsed)
                }
            }.bind(this))
        },

        verifyOZEFile: function(allowed, parsed) {
            return new Promise(function(resolve, reject) {
                var bits = this.filenameSplitter(parsed.filename)
                var date = bits[2].split('-')
                var year
                if (date[1] < 8)
                    year = date[2] - 1
                else
                    year = date[2]

                var uaiinfile = ''
                var line = 0
                while (uaiinfile == '' && line < parsed.data.data.length) {
                    uaiinfile = parsed.data.data[line]['Classes'].split('$')[0].toLowerCase()
                    line++
                }
                if (allowed.uai != uaiinfile) {
                    reject(Error(ERR.BAD_SCHOOL))
                }
                else if (!this.verifySchoolYear(year)) {
                    reject(Error(ERR.BAD_YEAR))
                }
                else {
                    resolve({time: this.timestampPHP(parsed.lastdate), type: 'tous'})
                    this.promise = this.processOZEFile(parsed)
                }
            }.bind(this))
        },

        verifyKosmosFile: function(allowed, parsed) {
            return new Promise(function(resolve, reject) {
                var bits = this.filenameKosmosSplitter(parsed.filename)
                var date = new Date(parsed.lastdate)
                var y = date.getFullYear()
                var m = date.getMonth() + 1
                var year
                if (m < 8)
                    year = y - 1
                else
                    year = y

                if (allowed.uai != bits[2].toLowerCase()) {
                    reject(Error(ERR.BAD_SCHOOL))
                }
                else if (!this.verifySchoolYear(year)) {
                    reject(Error(ERR.BAD_YEAR))
                }
                else {
                    resolve({time: this.timestampPHP(parsed.lastdate), type: 'tous'})
                    this.promise = this.processKosmosFile(parsed)
                }
            }.bind(this))
        },

        verifyLilieFile: function(allowed, parsed) {
            return new Promise(function(resolve, reject) {
                var bits = this.filenameSplitter(parsed.filename)
                var y = bits[2].substr(0, 4)
                var m = bits[2].substr(4, 2)
                var year
                if (m < 8)
                    year = y - 1
                else 
                    year = y

                var profil, type
                switch (bits[1]) {
                    case 'Eleve':
                        profil = 'student'
                        type = 'eleves'
                        break
                    case 'Elève':
                        profil = 'student'
                        type = 'eleves'
                        break
                    case 'Elève':
                        profil = 'student'
                        type = 'eleves'
                        break
                    case 'El%C3%A8ve':
                        profil = 'student'
                        type = 'eleves'
                        break
                    case 'Enseignant':
                        profil = 'teacher'
                        type = 'profs'
                        break
                    case 'Personnel':
                        profil = 'teacher'
                        type = 'personnels'
                        break
                    default:
                        reject(Error(ERR.BAD_ROLE))
                        return
                }

                if (allowed.special !== null && allowed.special !== bits[4]) {
                    reject(Error(ERR.BAD_SCHOOL))
                }
                else if (!this.verifySchoolYear(year)) {
                    reject(Error(ERR.BAD_YEAR))
                }
                else {
                    var response = {}
                    if (allowed.special === null)
                        response.special = bits[4]
                    response.time = this.timestampPHP(parsed.lastdate)
                    response.type = type
                    resolve(response)
                    this.promise = this.processLilieFile(profil, parsed)
                }
            }.bind(this))
        },

        verifyBE1DFile: function(parsed) {
            return new Promise(function(resolve, reject) {
                var bits = this.filenameSplitter(parsed.filename)
                var date = new Date(parsed.lastdate)
                var y = date.getFullYear()
                var m = date.getMonth() + 1
                var year
                if (m < 8)
                    year = y - 1
                else 
                    year = y
                if (!this.verifySchoolYear(year)) {
                    reject(Error(ERR.BAD_YEAR))
                }
                else {
                    resolve({time: this.timestampPHP(parsed.lastdate), type: 'eleves'})
                    this.promise = this.processBE1DFile(parsed)
                }
            }.bind(this))
        },

        processSiecleFile: function(parsed) {
            return new Promise(function(resolve, reject) {
                var profil = 'student'
                var $students = $(parsed.data).find('ELEVES:first').children()
                var $structures = $(parsed.data).find('STRUCTURES:first')

                var users = []
                $students.each(function(index) {
                    var cohorts = []
                    $structures.find('STRUCTURES_ELEVE[ELEVE_ID="' + $(this).attr('ELEVE_ID') + '"]').children().each(function() {
                        cohorts.push($(this).find('CODE_STRUCTURE').text())
                    })
                    
                    if (cohorts.length) {
                        var lastname = $(this).find('NOM_DE_FAMILLE').text()
                        if (lastname === '')
                            lastname = $(this).find('NOM').text()
                        users.push({
                            profil: profil,
                            cohorts: cohorts,
                            siecle: $(this).attr('ELEVE_ID'),
                            lastname: lastname,
                            firstname: $(this).find('PRENOM').text()
                        })
                    }
                })
                resolve({profil: profil, cas: false, users: users})
            })
        },

        processENTCOREFile: function(parsed) {
            return new Promise(function(resolve, reject) {
                var users = []
                for (var index in parsed.data.data) {
                    var user = parsed.data.data[index]

                    var profil
                    switch (user.Type) {
                        case 'Elève':
                            profil = 'student'
                            break
                        case 'Élève':
                            profil = 'student'
                            break
                        case 'Enseignant':
                            profil = 'teacher'
                            break
                        case 'Personnel':
                            profil = 'teacher'
                            break
                        default:
                            profil = null
                    }

                    var prenom = ''
                    if (typeof user.Prénom === 'undefined')
                        prenom = user.PrǸnom
                    else
                        prenom = user.Prénom

                    if (profil === 'student') {
                        var cohorts = []
                        if (user["Classes"] !== '')
                            cohorts.push(user["Classes"])
                        if (cohorts.length) {
                            users.push({
                                profil: profil,
                                username: user.Identifiant,
                                firstname: prenom,
                                lastname: user.Nom,
                                siecle: user["Identifiant SIECLE"],
                                cohorts: cohorts
                            })
                        }
                    }
                    else if (profil === 'teacher') {
                        users.push({
                            profil: profil,
                            username: user.Identifiant,
                            firstname: prenom,
                            lastname: user.Nom
                        })
                    }
                }
                resolve({profil: 'student', cas: true, users: users})
            })
        },
        
        processENTCORESPECIALFile: function(parsed) {
            return new Promise(function(resolve, reject) {
                var users = []
                for (var index in parsed.data.data) {
                    var user = parsed.data.data[index]

                    var profil
                    switch (user.Type) {
                        case 'Elève':
                            profil = 'student'
                            break
                        case 'Élève':
                            profil = 'student'
                            break
                        case 'Enseignant':
                            profil = 'teacher'
                            break
                        case 'Personnel':
                            profil = 'teacher'
                            break
                        default:
                            profil = null
                    }

                    var prenom = ''
                    if (typeof user.Prénom === 'undefined')
                        prenom = user.PrǸnom
                    else
                        prenom = user.Prénom

                    if (profil === 'student') {
                        var cohorts = []
                        if (user["Classes"] !== '')
                            cohorts.push(user["Classes"])
                        if (cohorts.length) {
                            users.push({
                                profil: profil,
                                username: user.Identifiant,
                                firstname: prenom,
                                lastname: user.Nom,
                                siecle: user["Identifiant SIECLE"],
                                cohorts: cohorts,
                                lilie: user.Id
                            })
                        }
                    }
                    else if (profil === 'teacher') {
                        users.push({
                            profil: profil,
                            username: user.Identifiant,
                            firstname: prenom,
                            lastname: user.Nom,
                            lilie: user.Id
                        })
                    }
                }
                resolve({profil: 'student', cas: true, users: users})
            })
        },

        processITOPFile: function(profil, parsed) {
            return new Promise(function(resolve, reject) {
                if (profil === 'student') {
                    var nbFields = 0
                    var fieldExist = true
                    while (fieldExist) {
                        if (parsed.data.meta.fields.indexOf('cohort' + (nbFields+1)) === -1)
                            fieldExist = false
                        else
                            nbFields++
                    }
                }
                var users = []
                for (var index in parsed.data.data) {
                    var user = parsed.data.data[index]
                    var cohorts = []
                    if (profil === 'student') {
                        for (var i = 0; i < nbFields; i++) {
                            if (user['cohort' + (i+1)] !== '')
                                cohorts.push(user['cohort' + (i+1)])
                        }
                        if (cohorts.length) {
                            users.push({
                                profil: profil,
                                username: user.username,
                                firstname: user.firstname,
                                lastname: user.lastname,
                                email: user.email,
                                cohorts: cohorts,
                                special: user.email
                            })
                        }
                    }
                    else {
                        users.push({
                            profil: profil,
                            username: user.username,
                            firstname: user.firstname,
                            lastname: user.lastname,
                            email: user.email
                        })
                    }
                }
                resolve({profil: profil, cas: true, users: users})
            })
        },

        processOZEFile: function(parsed) {
            return new Promise(function(resolve, reject) {

                var users = []
                for (var index in parsed.data.data) {
                    var user = parsed.data.data[index]

                    var profil
                    switch (user.Profil) {
                        case 'National_1':
                            profil = 'student'
                            break
                        case 'National_2':
                            profil = null
                            break
                        default:
                            profil = 'teacher'
                    }

                    var prenom = ''
                    if (typeof user.Prénom === 'undefined')
                        prenom = user.PrǸnom
                    else
                        prenom = user.Prénom

                    if (profil === 'student') {
                        var cohorts = []
                        if (user.Classes !== '') {
                            var cohortname = user.Classes.split('$')[1]
                            if (cohortname !== '') {
                                cohorts.push(cohortname)
                            }
                        }
                        if (user.Groupe !== '') {
                            var groups = user.Groupe.split(',')
                            groups.forEach(function (group) {
                                var cohortname = group.split('$')[1]
                                if (cohortname !== '') {
                                    cohorts.push(cohortname)
                                }
                            })
                        }
                        if (cohorts.length) {
                            users.push({
                                profil: profil,
                                username: user.Guid,
                                firstname: prenom,
                                lastname: user.Nom,
                                cohorts: cohorts
                            })
                        }
                    }
                    else if (profil === 'teacher') {
                        users.push({
                            profil: profil,
                            username: user.Guid,
                            firstname: prenom,
                            lastname: user.Nom
                        })
                    }
                }
                resolve({profil: 'student', cas: true, users: users})
            })
        },

        processKosmosFile: function(parsed) {
            return new Promise(function(resolve, reject) {

                var users = []
                for (var index in parsed.data.data) {
                    var user = parsed.data.data[index]

                    var profil
                    switch (user.profil) {
                        case 'Eleve':
                            profil = 'student'
                            break
                        case 'Enseignant':
                            profil = 'teacher'
                            break
                        case 'Personnel':
                            profil = 'teacher'
                            break
                        default:
                            profil = null
                    }

                    if (profil === 'student') {
                        if (user.classe != '') {
                            users.push({
                                profil: profil,
                                username: user.uid.toLowerCase(),
                                firstname: user.prenom,
                                lastname: user.nom,
                                cohorts: [user.classe]
                            })
                        }
                    }
                    else if (profil === 'teacher') {
                        users.push({
                            profil: profil,
                            username: user.uid.toLowerCase(),
                            firstname: user.prenom,
                            lastname: user.nom
                        })
                    }
                }
                resolve({profil: 'student', cas: true, users: users})
            })
        },

        processLilieFile: function(profil, parsed) {
            return new Promise(function(resolve, reject) {
                var users = []
                for (var index in parsed.data.data) {
                    var user = parsed.data.data[index]

                    if (profil === 'student') {
                        var cohorts = []
                        if (user.Classe !== '')
                            cohorts.push(user.Classe)
                        if (cohorts.length) {
                            users.push({
                                profil: profil,
                                username: user.Identifiant,
                                firstname: user.Prénom,
                                lastname: user.Nom,
                                siecle: user.Sconet,
                                cohorts: cohorts
                            })
                        }
                    }
                    else {
                        users.push({
                            profil: profil,
                            username: user.Identifiant,
                            firstname: user.Prénom,
                            lastname: user.Nom
                        })
                    }
                }
                resolve({profil: profil, cas: true, users: users})
            })
        },

        processBE1DFile: function(parsed) {
            return new Promise(function(resolve, reject) {
                var profil = 'student'
                var users = []
                for (var index in parsed.data.data) {
                    var user = parsed.data.data[index]
                    
                    var cohorts = []
                    if (user['Libellé classe'] !== '')
                        cohorts.push(user['Libellé classe'])
                    if (cohorts.length) {
                        users.push({
                            profil: profil,
                            firstname: user['Prénom élève'],
                            lastname: user['Nom élève'],
                            cohorts: cohorts,
                            special: user['Date naissance']
                        })
                    }
                }
                resolve({profil: profil, cas: false, users: users})
            })
        },

        filenameSplitter: function(filename) {
            return filename.split(/[_.]{1}/)
        },

        filenameKosmosSplitter: function(filename) {
            return filename.split(/[-.]{1}/)
        },
        
        timestringSplitter: function(string) {
            return string.split(/[/ :]{1}/)
        },
        
        timestampPHP: function(timestampJS) {
            return Math.round(timestampJS / 1000)
        },

        verifySchoolYear: function(year) {
            var d = new Date()
            var y = d.getFullYear()
            var m = d.getMonth() + 1

            if (m < 8)
                ++year

            return y == year ? true : false;
        }

    }

    var dataTransfer = {

        action: function() {
            return ajax.get('?way=import.ajax.init').then(ajax.handle)
        },

        special: function(special) {
            return ajax.post('?way=import.ajax.special', {special: special}).then(ajax.handle)
        },

        timestamp: function(timestamp, type) {
            return ajax.post('?way=import.ajax.timestamp', {timestamp: timestamp, type: type}).then(ajax.handle)
        },

        fileerror: function(fileerror) {
            return ajax.post('?way=import.ajax.update', {type: 'fileerror', data: fileerror}).then(ajax.handle)
        },

        go: function() {
            return ajax.post('?way=import.ajax.update', {type: 'launchtime'}).then(ajax.handle)
        },

        cohorts: function(cohort) {
            // il faudra afficher les erreurs réseaux ou renvoyer la requête !
            return ajax.post('?way=import.ajax.cohort', cohort).then(ajax.handle)
        },

        user: function(user) {
            // il faudra afficher les erreurs réseaux ou renvoyer la requête !
            return ajax.post('?way=import.ajax.user', user).then(ajax.handle)
        },

        finish: function() {
            return ajax.post('?way=import.ajax.update', {type: 'finishtime'}).then(ajax.handle)
        }

    }

    var listing = {

        loading: function(data) {
            var steps = []
            if (data.users.length !== 0) {
                if (data.profil === 'student') {
                    this.cohorts = this.getCohortsFromStudents(data.users)
                    steps.push('cohorts')
                }
                this.cas = data.cas
                this.users = data.users
                steps.push('users')
            }
            steps.push('report')
            return steps
        },

        getCohortsFromStudents: function(students) {
            var cohorts = []
            // remplacer par for of dès que netbeans arrêtera de gueuler !
            for (var index in students) {
                if (typeof students[index].cohorts !== 'undefined') {
                    for (var cohort in students[index].cohorts) {
                        // remplacer "indexOf" par "includes" lorsque ECMAScript7
                        //if (!cohorts.includes(cohort)) cohorts.push(cohort)
                        if (cohorts.indexOf(students[index].cohorts[cohort]) === -1)
                            cohorts.push(students[index].cohorts[cohort])
                    }
                }
            }
            return cohorts.sort()
        },

        buildIDsArray: function(users) {
            var report = {}
            for (var index in users) {
                if (typeof users[index].username !== 'undefined') {
                    if (typeof report[users[index].cohorts[0]] === 'undefined')
                        report[users[index].cohorts[0]] = []
                    report[users[index].cohorts[0]].push({
                        firstname: users[index].firstname,
                        lastname: users[index].lastname,
                        username: users[index].username,
                        password: users[index].password
                    })
                }
            }
            return report
        },

        buildIDsPDF: function(users) {
            var report = this.buildIDsArray(users)
            var content = []
            for (var index in report) {
                var students = report[index]
                var body = []
                var row = []
                for (var i in students) {
                    var student = students[i]
                    row.push({
                        text: [
                            {text: student.firstname + '\n' + student.lastname + '\n', bold: true},
                            student.username + '\n',
                            student.password
                        ],
                        style: 'tableCells'
                    })
                    if (row.length === 3) {
                        body.push(row)
                        row = []
                    }
                }
                if (row.length !== 0) {
                    for (var j = row.length; j < 3; j++)
                        row.push({text: ''})
                    body.push(row)
                }
                content.push(
                    {text: index, style: 'header'})
                    content.push({
                        table: {
                            dontBreakRows: true,
                            widths: [160, 160, 160],
                            body: body
                        },
                        pageBreak: 'after'
                    }
                )
            }
            return {
                content: content,
                styles: {
                    header: {
                        fontSize: 14,
                        bold: true,
                        margin: [0, 0, 0, 10]
                    },
                    tableCells: {
                        fontSize: 12,
                        margin: [10, 10, 10, 10]
                    }
                }
            }
        }

    }

    var controller = {

        modified: 0,
        source: '',
        updtpasswd: false,

        upload : function(file) {
            dataTransfer.action().then(function(allowed) {
                this.source = allowed.source
                var charset
                if (allowed.source === 'lilie' || allowed.source === 'primaire' || allowed.source ==='kosmos')
                    charset = 'ISO-8859-1'
                else
                    charset = 'UTF-8'
                return Promise.all([
                    Promise.resolve(allowed),
                    fileOpener.loading(file, charset)
                ])
            }).then(function(responses) {
                console.log(responses[0], responses[1])
                return dataRecognizer.loading(responses[0], responses[1])
            }).then(function(warnings) {
                if (typeof warnings.special !== 'undefined')
                    dataTransfer.special(warnings.special)
                return dataTransfer.timestamp(warnings.time, warnings.type)
            }).then(function(response) {
                if (response == 'EQUAL_THAN_PREV')
                    $('#equal-than-prev').show()
                if (this.source !== 'secondaire' && this.source !== 'primaire')
                    $('#registration-btn').trigger('click')
                else {
                    //viewModifier.$uploadValidate("Deux possibilités s'offrent à vous !", GO_ON)
                    $('#step-upload-validate').show().find('.panel').hide()
                    $('#registration-btn').show()
                    $('#updtpasswd-btn').show()
                }
            }).catch(function(error) {
                for (var index in ERR) {
                    if (error.message === ERR[index]) {
                        dataTransfer.fileerror(index)
                    }
                }
                viewModifier.$uploadValidate(error.message)
            })
        },

        registration: function(updtpasswd) {
            this.updtpasswd = typeof updtpasswd !== 'undefined' ? updtpasswd : false
            dataRecognizer.promise.then(function(response) {
                dataTransfer.go()
                viewModifier.$disableStep('upload')
                this.stepByStep(listing.loading(response))
            }.bind(this))
        },

        stepByStep: function(steps) {
            var step = steps.shift()
            if (typeof step !== 'undefined') {
                this[step]().then(function() {
                    setTimeout(function() { this.stepByStep(steps) }.bind(this), TIMEOUT)
                }.bind(this))
            }
        },

        cohorts: function() {
            viewModifier.$displayCohorts(listing.cohorts)
            viewModifier.$changeStep('cohorts')

            var promises = []
            for (var cohort in listing.cohorts) {
                promises.push(dataTransfer.cohorts({
                    name: listing.cohorts[cohort],
                    viewid: cohort
                }).then(function(response) {
                    viewModifier.$updateCohort(response.action, response.status)
                }))
            }
            return Promise.all(promises)
        },

       users: function() {
            viewModifier.$changeStep('users')

            var nbusers = listing.users.length

            return listing.users.reduce(function(sequence, user) {
                return sequence.then(function() {
                    if (listing.cas === true) {
                        return Promise.resolve('password')
                    }
                    else {
                        user.password = ManagePasswords.randomPassword(0,5,2,1)
                        return ManagePasswords.hashPassword(user.password)
                    }
                }).then(function(hash) {
                    var userFS = {}
                    for (var i in user) {
                        if (user[i] !== 'undefined' && i !== 'password')
                            userFS[i] = user[i]
                        userFS['updtpasswd'] = this.updtpasswd
                    }
                    userFS.hash = hash
                    console.log(userFS)
                    return dataTransfer.user(userFS)
                }.bind(this)).then(function(response) {
                    if (listing.cas === false && (response.status === 'created' || (this.updtpasswd && response.status === 'updated'))) {
                        this.modified++
                        user.username = response.action
                    }
                    viewModifier.$updateStudentsProgress(response.status, nbusers)
                }.bind(this))
            }.bind(this), Promise.resolve())
        },

        report: function() {
            dataTransfer.finish()
            if (listing.cas === false && this.modified) {
                this.PDF = listing.buildIDsPDF(listing.users)
                viewModifier.$adaptStepReport()
            }
            viewModifier.$changeStep('report')
            return Promise.resolve()
        }

    }

    viewModifier.$loadNavEvents()
    $('#loading').hide()
    $('#step-upload').show()

    $('#xmlfile-container').on({
        dragover: function() {
            $(this).css('border', '2px dashed red')
        },
        dragleave: function() {
            $(this).css('border', '2px dashed #BBB')
        },
        drop: function(e) {
            e.preventDefault()
            if (e.originalEvent.dataTransfer.files.length) {
                $(this).css('border', '2px dashed green')
                controller.upload(e.originalEvent.dataTransfer.files[0])
            }
            else {
                $(this).css('border', '2px dashed #BBB')
            }
        }
    })

    $('#xmlfile').change(function() {
        if (this.files.length) {
            $('#xmlfile-container').css('border', '2px dashed green')
            controller.upload(this.files[0])
        }
        else {
            $('#xmlfile-container').css('border', '2px dashed #BBB')
        }
    })

    $('#xmlfile-btn').click(function() {
        $('#xmlfile').trigger('click');
    })

    $('#registration-btn').click(function() {
        controller.registration()
    })
    
    $('#updtpasswd-btn').click(function() {
        controller.registration(true)
    })
    
    $('#save-btn').click(function() {
        pdfMake.createPdf(controller.PDF).download('elea-passwords.pdf')
    })

    $('#print-btn').click(function() {
        pdfMake.createPdf(controller.PDF).print()
    })

    $('#new-btn').click(function() {
        document.location.href = document.location.href
    })
    
})
