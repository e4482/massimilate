var ManagePasswords = {
    
    randomPassword: function(nbupper, nblower, nbnumbers, nbsymbols) {
        var upperletters = ["A","B","C","D","E","F","G","H","J","K","L","M","N","P","R","S","T","U","V","W","X","Y","Z"]
        var lowerletters = ["a","b","c","d","e","f","g","h","j","k","l","m","n","p","r","s","t","u","v","w","x","y","z"]
        var numbers = ["0","1","2","3","4","5","6","7","8","9"]
        var symbols = ["!"]
	var password = ""
	for (i = 0; i < nbupper; i++) {password = password + upperletters[Math.floor(Math.random() * upperletters.length)]}
        for (i = 0; i < nblower; i++) {password = password + lowerletters[Math.floor(Math.random() * lowerletters.length)]}
        for (i = 0; i < nbnumbers; i++) {password = password + numbers[Math.floor(Math.random() * numbers.length)]}
        for (i = 0; i < nbsymbols; i++) {password = password + symbols[Math.floor(Math.random() * symbols.length)]}
	return password
    },
    
    hashPassword: function(password) {
        return new Promise(function(resolve, reject){
            var bcrypt = new bCrypt()
            bcrypt.hashpw(password, bcrypt.gensalt(10), resolve)
        })
    }
    
}