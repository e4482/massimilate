var ManageSteps = {
    
    $loadNavEvents: function() {
        var that = this
        this.steps.forEach(function(step) {
            var stepNavID = '#nav-' + step
            
            $(stepNavID).click(step, function(e) {
                e.preventDefault()
                if (!$(this).hasClass('disabled')) that.$changeStep(e.data)
            })
        })
    },
    
    $disableStep: function(step) {
        var stepNavID = "#nav-" + step
        $(stepNavID).addClass('disabled')
    },
    
    $changeStep: function(newStep) {
        $('#mooring-navbar .active').removeClass('active')
        this.steps.forEach(function(step) {
            var stepNavID = "#nav-" + step
            var stepID = "#step-" + step
            if (step === newStep) {
                $(stepNavID).removeClass('disabled').addClass('active')
                $(stepID).show()
            } else {
                $(stepID).hide()
            }
        })
    },
    
    $stepValidate: function(id, type, title, msg, authorize) {
        authorize = typeof authorize !== 'undefined' ? authorize : 0

        var $step = $(id)
        var panel = 'panel-' + type
        $step.find('.panel').removeClass('panel-success panel-warning panel-danger').addClass(panel)
        $step.find('.panel-title').text(title)
        $step.find('.panel-body').html(msg)

        authorize ? $step.find('input').show() : $step.find('input').hide()

        $step.show()
    }
    
}