// This file is part of Mooring.
// 
// Mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Mooring.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Ajax library
 *
 * @package     mooring
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require(['local_mooring/URI'], function(URI) {
    
    this.ajax = {
        
        getText: function(url) {
            return this.request('GET', url, null, false)
        },
        
        getJSON: function(url, data) {
            url = URI(url).normalizeQuery().addQuery(data).toString()
            return this.request('GET', url, null, true)
        },
        
        postJSON: function(url, data) {
            url = URI(url).normalizeQuery().toString()
            var formData = this.toFormData(data)
            return this.request('POST', url, formData, true)
        },
        
        postForm: function(url, form) {
            url = URI(url).normalizeQuery().toString()
            var formData = new FormData(form)
            return this.request('POST', url, formData, true)
        },

        request: function(type, url, formData, json) {
            return new Promise(function(resolve, reject) {
                var request = new XMLHttpRequest()
                request.open(type, url)
                request.onload = function() {
                    if (request.status === 200) {
                        if (json)
                            resolve(JSON.parse(request.responseText))
                        else
                            resolve(request.responseText)
                    }
                    else
                        reject(Error(request.statusText))
                }
                request.onerror = function() {
                    reject(Error("La communication avec le serveur a échoué !"))
                }
                request.send(formData)
            })
        },

        toFormData: function(data) {
            var formData = new FormData()
            for (var index in data) {
                if (typeof data[index] === 'object') {
                    for (var i in data[index]) {
                        if (
                            typeof data[index][i] === 'string'
                            || typeof data[index][i] === 'number'
                            || typeof data[index][i] === 'boolean'
                        )
                            formData.append(index + '[]', data[index][i])
                    }
                }
                else if (
                    typeof data[index] === 'string'
                    || typeof data[index] === 'number'
                    || typeof data[index] === 'boolean'
                )
                    formData.append(index, data[index])
            }
            return formData
        }

    }
    
}.bind(this))