// This file is part of Mooring.
// 
// Mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Mooring.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Password creation library
 *
 * @package     mooring
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

var passwordCreation = {
    
    randomPassword: function(nbupper, nblower, nbnumbers, nbsymbols) {
        var upperletters = ["A","B","C","D","E","F","G","H","J","K","L","M","N","P","R","S","T","U","V","W","X","Y","Z"]
        var lowerletters = ["a","b","c","d","e","f","g","h","j","k","l","m","n","p","r","s","t","u","v","w","x","y","z"]
        var numbers = ["0","1","2","3","4","5","6","7","8","9"]
        var symbols = ["!"]
	var password = ""
	for (i = 0; i < nbupper; i++) {password = password + upperletters[Math.floor(Math.random() * upperletters.length)]}
        for (i = 0; i < nblower; i++) {password = password + lowerletters[Math.floor(Math.random() * lowerletters.length)]}
        for (i = 0; i < nbnumbers; i++) {password = password + numbers[Math.floor(Math.random() * numbers.length)]}
        for (i = 0; i < nbsymbols; i++) {password = password + symbols[Math.floor(Math.random() * symbols.length)]}
	return password
    },
    
    hashPassword: function(password) {
        return new Promise(function(resolve, reject){
            var bcrypt = new bCrypt()
            bcrypt.hashpw(password, bcrypt.gensalt(10), resolve)
        })
    }
    
}