// This file is part of Mooring.
// 
// Mooring is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Mooring is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Mooring.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Steps management library
 *
 * @package     mooring
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

var stepsManagement = {
    
    $loadNavEvents: function() {
        var that = this
        this.steps.forEach(function(step) {
            var stepNavID = '#nav-' + step
            
            $(stepNavID).click(step, function(e) {
                e.preventDefault()
                if (!$(this).hasClass('disabled')) that.$changeStep(e.data)
            })
        })
    },
    
    $disableStep: function(step) {
        var stepNavID = "#nav-" + step
        $(stepNavID).addClass('disabled')
    },
    
    $changeStep: function(newStep) {
        $('#moolinette-navbar .active').removeClass('active')
        this.steps.forEach(function(step) {
            var stepNavID = "#nav-" + step
            var stepID = "#step-" + step
            if (step === newStep) {
                $(stepNavID).removeClass('disabled').addClass('active')
                $(stepID).show()
            } else {
                $(stepID).hide()
            }
        })
    },
    
    $stepValidate: function(id, type, title, msg, authorize) {
        authorize = typeof authorize !== 'undefined' ? authorize : 0

        var $step = $(id)
        var panel = 'panel-' + type
        $step.find('.panel').removeClass('panel-success panel-warning panel-danger').addClass(panel)
        $step.find('.panel-title').text(title)
        $step.find('.panel-body').html(msg)

        authorize ? $step.find('input').show() : $step.find('input').hide()

        $step.show()
    }
    
}