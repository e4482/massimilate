require(['local_mooring/ajax'], function(ajax) {
    $('.delete-member').click(function () {
        // transfer userid and cohortid data to modal for ajax purpose
        var data = $(this).data()
        $('#cohortmember-deletion-confirmation').data(data)

        // change title
        var textbody = $('#cohortmember-deletion-template').html()
        textbody = textbody
            .replace(/{{username}}/g, data.username)
            .replace(/{{cohortname}}/g, data.cohortname)
        $('#cohortmember-deletion-body').html(textbody)

        $('#cohortmember-deletion').modal({
            show: 'true',
            backdrop: true,
            keyboard: true
        });


    })

    $('#cohortmember-deletion-confirmation').click(function() {
        $('#cohortmember-deletion').modal('hide')

        var data = $(this).data()
        console.log('Suppression utilisateur ' + data.userid + ' de la cohorte ' + data.cohortid)
        ajax.post('?way=cohorts.ajax.unsubscribe', data).then(ajax.handle).then(function (response) {
            if (response.status === 'unsubscribed') {
                $('.tile-user[data-userid=' + data.userid + ']').parent().hide()
            }
        })
    })


})