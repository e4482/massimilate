<?php

// This file is part of Massimilate.
// 
// Massimilate is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Massimilate is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Massimilate.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Upgrade
 *
 * @package     local_massimilate
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

function xmldb_local_massimilate_upgrade($oldversion) {

    global $DB;
    $dbman = $DB->get_manager();

    if ($oldversion < 2018092000) {

        // Define table local_massimilate_action to be dropped.
        $table = new xmldb_table('local_massimilate_action');

        // Conditionally launch drop table for local_massimilate_action.
        if ($dbman->table_exists($table)) {
            $dbman->drop_table($table);
        }

        // Massimilate savepoint reached.
        upgrade_plugin_savepoint(true, 2018092000, 'local', 'massimilate');
    }
    
    return true;

}
