# MASSIMILATE
## I will oversee the assimilation of countless million users !

-----

## Requirements

### Needed Role

You must create a new role called "schoolmanager" :

        global $DB;
        if (!$DB->record_exists('role', ['shortname' => 'schoolmanager'])) {
            $roleid = create_role("Gestionnaire établissement", 'schoolmanager', "created by local_massimilate");
            assign_capability('local/massimilate:import', CAP_ALLOW, $roleid, context_system::instance());
            assign_capability('local/massimilate:cohorts', CAP_ALLOW, $roleid, context_system::instance());
        }

You can run this code in db/install.php file for automation :

        function xmldb_local_massimilate_install() {
            // above code
        }

### Bower via npm (as root)

        npm install -g bower

## Installation

### Prepare AMD JS files

        npm install
        bower install
        grunt install
        rm -rf node_modules
        rm -rf bower_components