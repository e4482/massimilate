// This file is part of Massimilate.
// 
// Massimilate is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Massimilate is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Massimilate.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Grunt script
 *
 * @package     local_massimilate
 * @author      Rémi Lefeuvre
 * @copyright   2014 Andrew Nicols
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

module.exports = function(grunt) {
    
    var path = require('path'),
        cwd = process.env.PWD || process.cwd()

    // PHP strings for exec task.
    var moodleroot = path.dirname(path.dirname(__dirname)),
        configfile = '',
        decachephp = '',
        dirrootopt = grunt.option('dirroot') || process.env.MOODLE_DIR || '';

    // Allow user to explicitly define Moodle root dir.
    if ('' !== dirrootopt) {
        moodleroot = path.resolve(dirrootopt);
    }
    
    configfile = path.join(moodleroot, 'config.php');

    decachephp += 'define(\'CLI_SCRIPT\', true);';
    decachephp += 'require(\'' + configfile + '\');';
    decachephp += 'js_reset_all_caches();';

    // Project configuration.
    grunt.initConfig({
        copy: {
            main: {
                files: [
                    {
                        expand: true,
                        cwd: 'bower_components/papaparse',
                        src: 'papaparse.min.js',
                        dest: 'amd/build/'
                    },
                    {
                        expand: true,
                        cwd: 'bower_components/pdfmake/build',
                        src: 'pdfmake.min.js',
                        dest: 'public/js/pdfmake/'
                    },
                    {
                        expand: false,
                        src: 'bower_components/pdfmake/build/vfs_fonts.js',
                        dest: 'public/js/pdfmake/vfs_fonts.min.js'
                    }
                ]
            }
        },
        exec: {
            decache: {
                cmd: 'php -r "' + decachephp + '"',
                callback: function(error) {
                    // exec will output error messages
                    // just add one to confirm success.
                    if (!error) {
                        grunt.log.writeln("Moodle js cache reset.");
                    }
                }
            }
        }
    });

    // Register NPM tasks.
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-exec');
    
    // Register tasks.
    grunt.registerTask('default', ['copy']);
    grunt.registerTask('decache', ['exec:decache']);
    grunt.registerTask('install', ['copy']);

};
