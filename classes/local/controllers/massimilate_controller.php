<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 11/04/19
 * Time: 14:26
 */

namespace local_massimilate\local\controllers;

use local_mooring\local\controllers\app_controller;
use local_mooring\local\library;

class massimilate_controller extends app_controller
{

    protected $uai;
    protected $mainpagelink;
    protected $cohortlink;


    public function __construct($capability) {
        parent::__construct();
        require_capability($capability, $this->context);

        $this->load_model('school_table', 'school');

        $uai = filter_input(INPUT_GET, 'uai', FILTER_SANITIZE_STRING);
        if (!$uai) {
            global $USER;
            $this->uai = $USER->username;
        } else {
            //TODO vérifier la délégation
            require_capability('local/massimilate:delegation', $this->context);
            $this->uai = $uai;
        }

        $this->mainpagelink = '?way=import.page.index';
        $this->cohortslink = '?way=cohorts.page.index';
        $this->cohortlink = '?way=cohorts.page.manage&cohort=';

        library::test_uai($this->uai);
    }

    protected function external_docs($method) {
        return new \moodle_url('/local/faq/.?role=' . $method);
    }

}