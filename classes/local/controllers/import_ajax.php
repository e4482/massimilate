<?php

// This file is part of Massimilate.
// 
// Massimilate is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Massimilate is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Massimilate.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Importation ajax controller
 *
 * @package     local_massimilate
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_massimilate\local\controllers;

use local_mooring\local\controllers\app_controller;
use local_mooring\local\models\app_cohort;
use local_mooring\local\models\app_user;
use local_mooring\local\library;

class import_ajax extends app_controller {
    
    public function __construct() {
        parent::__construct();
        require_capability('local/massimilate:import', $this->context);
        $this->load_model('import_table', 'import');
        $this->load_model('import_session', 'session');
    }
    
    public function init() {
        $this->session->id = $this->import->init($this->session->uai);
        
        $cohort = new app_cohort($this->session->uai);
        $this->session->cohorts = $cohort->all_indexed_by_name();
        
        $this->load_model('school_table', 'school');
        $school = $this->school->one($this->session->uai);
        
        return [
            'uai'       => $school->uai,
            'special'   => $school->special,
            'source'    => library::school_import_source($school)
        ];
    }
    
    public function special() {
        $special = filter_input(INPUT_POST, 'special', FILTER_SANITIZE_STRING, ['flags' => FILTER_FLAG_NO_ENCODE_QUOTES]);
        $this->load_model('school_table', 'school');
        return $this->school->special($this->session->uai, $special);
    }
    
    public function timestamp() {
        $data = (object) filter_input_array(INPUT_POST, [
            'timestamp' => FILTER_SANITIZE_STRING,
            'type'      => FILTER_SANITIZE_STRING
        ]);
        
        $data->uai = $this->session->uai;
        $data->id = $this->session->id;
        
        $status = $this->import->timestamp($data);
        $this->session->timestamp = $data->timestamp;
        return $status;
    }
    
    public function update() {
        $post = (object) filter_input_array(INPUT_POST, [
            'type'  => FILTER_SANITIZE_STRING,
            'data'  => FILTER_SANITIZE_STRING
        ]);
        return $this->import->update([
            'id'    => $this->session->id,
            'field' => $post->type,
            'new'   => $post->data
        ]);
    }
    
    public function cohort() {
        $data = (object) filter_input_array(INPUT_POST, [
            'name'      => FILTER_SANITIZE_STRING,
            'viewid'    => FILTER_VALIDATE_INT
        ]);
        
        if ($this->session->cohort_exists($data->name)) {
            $status = 'known';
        } else {
            $cohort = new app_cohort($this->session->uai);
            $cohortid = $cohort->create($data->name);
            $this->session->set_cohort($data->name, $cohortid);
            $status = 'added';
        }
        
        return [
            'status'    => $status,
            'action'    => $data->viewid
        ];
    }
    
    public function user() {
        $user = (object) filter_input_array(INPUT_POST, [
            'profil'    => FILTER_SANITIZE_STRING,
            'username'  => FILTER_SANITIZE_STRING,
            'lastname'  => FILTER_SANITIZE_STRING,
            'firstname' => FILTER_SANITIZE_STRING,
            'email'     => FILTER_VALIDATE_EMAIL,
            'hash'      => FILTER_SANITIZE_STRING,
            'siecle'    => FILTER_SANITIZE_STRING,
            'special'   => FILTER_SANITIZE_STRING,
            'lilie'     => FILTER_SANITIZE_STRING
        ]);
        $updtpasswd = filter_input(INPUT_POST, 'updtpasswd', FILTER_VALIDATE_BOOLEAN);
        $cohorts = filter_input(INPUT_POST, 'cohorts', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
        
        $this->load_model('school_table', 'school');
        $user->uai = $this->session->uai;
        $user->timestamp = $this->session->timestamp;
        $cas = $this->school->one($this->session->uai)->cas;
        
        $userobj = new app_user($user, $cas);
        $status = $userobj->update_or_create($updtpasswd);
        $userid = $userobj->get_userid();
        
        if ($status !== 'known' && is_array($cohorts)) {
            $cohort = new app_cohort($this->session->uai);
            $cohort->update_user($userid, $cohorts, $this->session->cohorts);
        }

        return [
            'status'    => $status,
            'action'    => $userobj->get_username()
        ];
    }
    
}
