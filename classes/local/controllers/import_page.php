<?php

// This file is part of Massimilate.
// 
// Massimilate is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Massimilate is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Massimilate.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Importation page controller
 *
 * @package     local_massimilate
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_massimilate\local\controllers;

use local_mooring\local\library;

class import_page extends massimilate_controller {

    public function __construct()
    {
        parent::__construct('local/massimilate:import');
    }

    public function index() {
        $school = $this->school->one($this->uai);
        
        $this->load_model('import_table', 'import');
        $imports = $this->import->status_all($school);
        
        $importlink = '?way=import.page.wizard';
        $cohortslink = '?way=cohorts.page.index';
        if (isset($_GET['uai'])) {
            $importlink .= '&uai=' . $this->uai;
            $cohortslink.= '&uai=' . $this->uai;
        }

        $tutorial = $this->external_docs(library::school_import_method($school));

        $this->render('import.index', compact('school', 'imports', 'importlink', 'cohortslink', 'tutorial'));
    }
    
    public function wizard() {
        $this->load_model('import_session', 'session');
        $this->session->clear();
        $this->session->uai = $this->uai;
        
        $link = '?way=import.page.index';
        if (isset($_GET['uai'])) {
            $link .= '&uai=' . $this->uai;
        }
        
        $this->render('import.wizard', compact('link'));
    }
    

}
