<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 09/04/19
 * Time: 14:29
 */

namespace local_massimilate\local\controllers;

use local_mooring\local\models\app_cohort;
use local_mooring\local\library;


global $CFG;
require_once($CFG->dirroot.'/cohort/locallib.php');

class cohorts_page extends massimilate_controller
{
    public function __construct()
    {
        parent::__construct('local/massimilate:cohorts');
    }

    public function index() {

        /*
         * - school
         * - cohorts (from school)
         * - cohortlink:  cohort page link
         */
        $school = $this->school->one($this->uai);
        $appcohort = new app_cohort($this->uai);
        $cohorts = $appcohort->all();
        $mainpagelink = $this->mainpagelink;
        $cohortlink = $this->cohortlink;
        $tutorial = $this->external_docs(library::school_import_method($school));


        $this->render('cohorts.index', compact('school', 'cohorts', 'cohortlink', 'mainpagelink', 'tutorial'));
    }

    public function manage() {

        /*
         * - cohort
         * - cohortmembers
         * - mainpagelink
         * - cohortslink
         */
        $this->load_model('core_session', 'session');
        $this->session->clear();
        $this->session->uai = $this->uai;

        $cohortname = filter_input(INPUT_GET, 'cohort', FILTER_SANITIZE_STRING);
        if ($cohortname===null) {
            // TODO message d'erreur
            throw new \Exception("Cohorte invalide");
        }

        $appcohort = new app_cohort($this->uai);
        $cohort = $appcohort->one($cohortname);
        if ($cohort===null) {
            throw new \Exception("Cohorte invalide");
        }

        $cohortmembers = $appcohort->get_users_from_cohort($cohort->id);

        $mainpagelink = $this->mainpagelink;
        $cohortslink = $this->cohortslink;

        $tutorial = $this->external_docs(library::school_import_method($this->school->one($this->uai)));

        $this->render('cohorts.manage', compact('cohort', 'cohortmembers', 'cohortslink', 'tutorial'));

    }

}