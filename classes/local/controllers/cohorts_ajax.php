<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 15/04/19
 * Time: 14:23
 */

namespace local_massimilate\local\controllers;

use local_mooring\local\controllers\app_controller;
use local_mooring\local\models\app_cohort;

class cohorts_ajax extends app_controller
{
    public function __construct()
    {
        parent::__construct();
        require_capability('local/massimilate:cohorts', $this->context);
        $this->load_model('core_session', 'session');
    }

    function unsubscribe() {
        $appcohort = new app_cohort($this->session->uai);

        $userid = filter_input(INPUT_POST, 'userid', FILTER_VALIDATE_INT);
        $cohortid = filter_input(INPUT_POST, 'cohortid', FILTER_VALIDATE_INT);

        $appcohort->unsuscribe_user($userid, $cohortid);

        $status = 'unsubscribed';

        return [
            'status' => $status
        ];
    }
}