<?php

// This file is part of Massimilate.
// 
// Massimilate is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Massimilate is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Massimilate.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Importation table model
 *
 * @package     local_massimilate
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_massimilate\local\models;

use local_mooring\local\models\core_table;
use local_mooring\local\config;
use local_mooring\local\library;

class import_table extends core_table {
    
    protected $table = 'local_massimilate_import';
    
    public function init($uai) {
        global $DB, $USER;
        return $DB->insert_record($this->table, [
            'username'      => $USER->username,
            'uai'           => $uai,
            'actiontime'    => time()
        ]);
    }
    
    public function timestamp($data) {
        global $DB;
        $results = $DB->get_records($this->table, [
            'uai'   => $data->uai,
            'type'  => $data->type
        ], 'launchtime DESC', 'timestamp, launchtime, finishtime', 0, 1);
        (count($results) && isset(reset($results)->launchtime)) ? $prev = reset($results)->timestamp : $prev = 0;
        if ($prev > $data->timestamp) {
            $data->fileerror = 'OLDER_THAN_PREV';
            $DB->update_record($this->table, $data);
            throw new \Exception("Un fichier plus récent a déjà été importé&nbsp;!");
        } else if ($prev == $data->timestamp) {
            if (reset($results)->finishtime) {
                $data->fileerror = 'PREVIOUS_SUCCESS';
                $DB->update_record($this->table, $data);
                throw new \Exception("Ce fichier a déjà été importé avec succès&nbsp;!");
            }
            $data->fileerror = 'EQUAL_THAN_PREV';
            $DB->update_record($this->table, $data);
            return 'EQUAL_THAN_PREV';
        }
        $DB->update_record($this->table, $data);
        return 'ALL OK';
    }
    
    public function update($data) {
        global $DB;
        $object = (object) $data;
        switch ($object->field) {
            case 'launchtime':
                $object->new = time();
                break;
            case 'finishtime':
                $object->new = time();
                break;
        }
        $array = ['id' => $object->id, $object->field => $object->new];
        return $DB->update_record($this->table, $array);
    }
    
    public function status_all($school) {
        $source = library::school_import_source($school);
        $types = config::load('source')->get($source, 'source');
        $imports = [];
        foreach ($types as $type => $name) {
            $imports[$type] = $this->status_one($school->uai, $type, $name);
        }
        return $imports;
    }
    
    public function status_one($uai, $type, $name) {
        global $DB;
        // get_records ne permet pas de préciser la troisième condition
        // get_records_list envisagée lorsqu'il y aura un nouveau champ "année scolaire" dans la table import
        $where = 'uai="' . $uai . '" AND type="' . $type . '" AND finishtime IS NOT NULL';
        $results = $DB->get_records_select($this->table, $where , null, 'finishtime DESC', 'id, timestamp', 0, 1);
        $import = (object) ['name' => $name];
        if (count($results)) {
            $timestamp = reset($results)->timestamp;
        } else {
            $timestamp = 0;
        }
        // Au cas où l'import est fait fin aout plutôt que début septembre
        $currentyeartimestamp = strtotime('first day of August');
        if ($currentyeartimestamp > time()) {
            $currentyeartimestamp = strtotime('last year', $currentyeartimestamp);
        }

        if ($timestamp > $currentyeartimestamp) {
            $import->class = 'success';
        } else if ($timestamp > strtotime('last year', $currentyeartimestamp)) {
            $import->class = 'warning';
        } else {
            $import->class = 'danger';
        }
        $import->timeago = library::timeago($timestamp);
        $import->timestamp = $timestamp;
        return $import;
    }
    
}
