<?php

// This file is part of Massimilate.
// 
// Massimilate is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Massimilate is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Massimilate.  If not, see <http://www.gnu.org/licenses/>.

/**
 * English language file
 *
 * @package     local_massimilate
 * @author      Rémi Lefeuvre
 * @copyright   (C) Rémi Lefeuvre 2016
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = "Massimilate";

$string['massimilate:import'] = "Importer les utilisateurs d'un établissement";
$string['massimilate:delegation'] = "Importer les utilisateurs des établissements";
$string['massimilate:cohorts'] = "Gestion restreinte des classes de l'établissement";

$string['mainpagelink'] = "Revenir à la page des imports";
$string['cohortmanagementlink'] = "Afficher les cohortes de l'établissement";


$string['cohortmemberdeletiontitle'] = "Retirer un élève de la cohorte";
$string['cohortmemberdeletiontext'] = "<p>Souhaitez-vous vraiment retirer l'élève <b>{{username}}</b> 
    de la cohorte <b>{{cohortname}}</b>&nbsp?</p>
    <p> Celui-ci sera automatiquement désinscrit des parcours auxquels la cohorte est inscrite.</p>
    <p>La désinscription est définitive. Pour réinscrire l'élève dans une cohorte, veuillez procéder à un nouvel import 
    des élèves depuis l'ENT.</p>";

$string['title'] = "Massimilate";
$string['heading'] = "Massimilate";
